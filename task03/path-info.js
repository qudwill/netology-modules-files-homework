const fs = require('fs');
const conf = { encoding: 'utf8' };

module.exports = (path, callback) => {
	fs.stat(path, (err, stats) => {
		if (err) return callback(err);
		
		let obj = {};

		obj.path = path;

		if (stats.isFile()) {
			obj.type = 'file';

			fs.readFile(path, conf, (err, content) => {
				if (err) return callback(err);
				
				obj.content = content;

				callback(err, obj);
			});
		} else if (stats.isDirectory()) {
			obj.type = 'directory';

			fs.readdir(path, conf, (err, childs) => {
				if (err) return callback(err);

				obj.childs = childs;

				callback(err, obj);
			});
		}
	});
};