const fs = require('fs');
const conf = { encoding: 'utf8' };

const getFileNames = path => {
	return new Promise((done, fail) => {
		fs.readdir(path, conf, (err, filenames) => {
			err ? fail(err) : done(filenames);
		});
	});
};

const getContent = (path, filename) => {
	return new Promise((done, fail) => {
		fs.readFile(`${path}${filename}`, conf, (err, content) => {
			err ? fail(err) : done(content);
		});
	});
};

module.exports = path => {
	return getFileNames(path)
		.then(filenames => Promise.all(filenames.map(filename => {
			return getContent(path, filename)
				.then(content => {
					return {content, filename};
				})
				.catch(err => console.log(err));
		}))
		.then(items => {
			return items.map(item => {
				return item ? {name: item.filename, content: item.content} : null;
			});
		})
		.catch(err => console.error(err)));
};