const fs = require('fs');
const conf = { encoding: 'utf8' };

const read = file => {
	return new Promise((done, fail) => {
		fs.readFile(file, conf, (err, data) => {
			err ? fail(err) : done(data);
		});
	});
};

const write = (file, data) => {
	return new Promise((done, fail) => {
		fs.writeFile(file, data, err => {
			err ? fail(err) : done(file);
		});
	});
};

module.exports = { read, write };